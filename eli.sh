#!/bin/bash
while getopts ":a:b:c:d:x:h" o; do case "${o}" in
	a) ./install.sh ;; #default
	b) ./install.sh -e yes ;; #encryption
	c) ./install.sh -b bios ;; #bios 
	d) ./install.sh -b bios -e yes ;; #bios and encryption
	x) ./install -f xfs ;; #xfs fs
	h) echo "This is the quick way to get going" &&exit ;;
	*) echo "-$OPTARG is not a valid option." && exit ;;
esac done

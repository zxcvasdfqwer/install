#!/bin/bash

#ALPHA level SOFT

NIX_VER=19.03

#This should only be used by people who are familiar with
#the nixos install process, because it will NOT 
#automatically work ootb
#trizen --noconfirm -S nix

function nixupdate (){
nix-channel -add https://nixos.org/channels/nixos-${NIX_VER} nixpkgs

nix-channel --update

nix-env -iE "_: with import <nixpkgs/nixos> { configuration = {}; }; with config.system.build; [ nixos-generate-config nixos-install nixos-enter manual.manpages ]"
}
#I would suggest you get nix the recommended way on their website, and NOT through the aur, because you will be missing functions should you install from the aur


function bootstrap(){
nixos-generate-config --root /mnt
nano /mnt/etc/nixos/configuration.nix #this cmd can be replaced with you copying over your configuration.nix file over to the same place, and you wouldn't need to manually edit it
nixos-install
	
	function instpack(){
		nix-env install $1
	}
	
	#the whole of the chroot script should be completely uneeded, because of how nixos installs itself with everything being done in your configuration.nix file
	#nixos uses systemd ootb 
}

#!/bin/bash
function inchroot(){

	function portage_setup () {
		emerge-webrsync

		#emerge --sync

		eselect profile set "default/linux/amd64/17.0"
	}

	function makeconf () {
echo '
CFLAGS="-march=native -O3"
CXXFLAGS="\${CFLAGS}"
CPU_FLAGS_X86="mmx mmxext sse sse2 ssse3 sse3"
MAKEOPTS="-j4"
PORTAGE_NICENESS=19
' >> /etc/portage/make.conf
	}

	function kernel () {
		emerge sys-kernel/gentoo-sources
		emerge sys-kernel/genkernel

		cd /usr/src/linux
		genkernel all
		genkernel --install initramfs

		emerge sys-kernel/linux-firmware
	}

	function updateworld(){

	emerge --ask --verbose --update --deep --newuse @world

	}

#steps in order
portage_setup
makeconf
kernel
updateworld
}
#instpack dhcpcd
#instpack iw wpa_supplicant

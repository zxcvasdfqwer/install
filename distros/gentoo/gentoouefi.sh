#!/bin/bash
function bootstrap(){

		function fetchtarballAndex() {
			builddate=$(curl -s http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64/ | sed -nr 's/.*href="stage3-amd64-([0-9].*).tar.xz">.*/\1/p')
			wget http://distfiles.gentoo.org/releases/amd64/autobuilds/current-stage3-amd64/stage3-amd64-$builddate.tar.xz
		cp stage3*.tar.xz /mnt/
		cd /mnt
		tar pxf stage3*.tar.xz 
		}
	
	fetchtarballAndex
	
	mirrorselect -i -o >> /mnt/gentoo/etc/portage/make.conf
	mkdir -p /mnt/gentoo/etc/portage/repos.conf
	cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf

	function instpack(){
		emerge $1
	}	

	function initenable(){
		rc-update add $1 default
	}		
	
	#function initstart(){
		#rc-update start $1
	#}

}

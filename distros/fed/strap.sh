#!/bin/bash

#CENTOS

#The default file system for centos is xfs, so make sure you use that with the -f flag
#Dependencies
#rpm
trizen --noconfirm -S rpm-org

#rpm --root /mnt -i http://mirror.centos.org/centos/7/os/x86_64/Packages/centos-release-7-${VER}.rpm
#may need to wget the centos rpm file first, because rpm does not like instlling from the URI
#wget ${RPMURI}
#rpm --root /mnt -i centos-*
#
#also for somereason, rpm will refuse to install packages on arch, I am not entirely sure why, but I really don't want to have to use fedora media to install centos.


function bootstrap(){
		VER=$(curl -s http://mirror.centos.org/centos/7/os/x86_64/Packages/ | sed -nr 's/.*href="centos-release-7-([0-9].*).rpm".*/\1/p')

		RPMURI="http://mirror.centos.org/centos/7/os/x86_64/Packages/centos-release-7-${VER}.rpm"

	rpm --root /mnt -i ${RPMURI}

	function instpack(){
		yum install $1
	}
	
	#uses systemd, so the same as arch for initenale and initstart

	
	export grubpkgname=grub2-efi
}


#inchroot command needs to be placed in the 'chroot' script.

function inchroot(){
	yum --releasever=7 install yum centos-release
	yum install @core @base redhad-lsb-core dracut-tools dracut-config-generic dracut-config-rescue

	#Place in the grub install function
	yum install grub2 grub2-efi efibootmgr

}

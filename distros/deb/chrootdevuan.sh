#!/bin/bash
#sets timezone
ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime

hwclock --systohc

#locale gen
#echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
#echo "en_US ISO-8859-1" >> /etc/locale.gen
#locale-gen

#network manager setup
apt install netbase net-tools ifupdown isc-dhcp-client inetutils-ping 
apt install networkmanager
#systemctl enable NetworkManager
#systemctl start NetworkManager

apt isntall dialog
dpkg-reconfigure debconf

apt-get install vim sudo initramfs-tools linux-image-amd64 linux-headers-amd64

yes | apt-get update
yes | apt-get upgrade

#this is just the grub install, for uefi 
apt install grub-efi efibootmgr os-prober


#mkdir /boot/efi
#mount /dev/sda1 /boot/efi
#grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --recheck
#grub-mkconfig -o /boot/grub/grub.cfg
#mkdir /boot/efi/EFI/BOOT
#cp /boot/efi/EFI/GRUB/grubx64.efi /boot/efi/EFI/BOOT/BOOTX64.EFI

passwd

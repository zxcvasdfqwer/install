#!/bin/bash
#this script is just to give you an idea on what you can do for the main install script, and modifying the bootstrap function
#if you were to use this in the bootstrap function of the archinstall script, you would definatley have to make some changes, because I am very unfamiliar with how debian bootstraps itself and how to setup grub properly on that system. 
#os=devuan
#hostname=devuan
#mirror= #see below for a mirror     #http://pkgmaster.devuan.org/merged
#alias initstart=systemctl start #replace this with the equivalent in openrc
#alias initenable= systemctl enable #replace this with the equivalent in openrc 
#fetch devuan's version of debootstrap, as it supports more stuff than the regular debootstrap
#
#this git clone could be avoided if you were just using regular debian, because you wouldn't need the patches that you would need for devuan

#deb mirror @ msu
#http://debian.cse.msu.edu/debian/
#devuan mirror
#http://pkgmaster.devuan.org/merged
function bootstrapdebian(){
debootstrap --arch=amd64 testing /mnt http://debian.cse.msu.edu/debian/

	function instpack(){
		apt install $1
	}
	
	#Debian uses systemd, so it is the same as arch
	#for initenable & initstart

	export grubpkgname=grub-efi
}

function bootstrapdevuan(){
#ToDo
git clone https://git.devuan.org/devuan-packages/debootstrap.git
cd debootstrap
export DEBOOTSTRAP_DIR=`pwd`
./debootstrap --arch=amd64 $release /mnt http://pkgmaster.devuan.org/merged

	function instpack(){
		apt install $1
	}

}

function bootstrapUbuntu(){
debootstrap --arch=amd64 bionic /mnt http://ports.ubuntu.com/ubuntu-ports/


	#ubuntu uses apt and systemD





}

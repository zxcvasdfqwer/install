#!/bin/bash

trizen -S xbps

#if you want musl-libc
#export XBPS_ARCH=x86_64-musl

#bootstraps void
function bootstrap(){
#the reason there is so much extra stuff in here is because the chroot script does not work as advertised I suppose
xbps-install -S -R http://mirrors.servercentral.com/voidlinux/current -r /mnt base-system lvm2 cryptsetup mkinitcpio mkinitcpio-encrypt mkinitcpio-xbps linux-firmware NetworkManager bash bash-completion 

	function instpack(){
		xbps-install -S $1 -y
	}
	
	function initenable(){
		ln -s /etc/sv/$1 /var/service/
	}
	
	function initstart(){
		sv up $1
	}

#export grubpkgname=grub-x86_64-efi #not needed, in bootstrap script
}

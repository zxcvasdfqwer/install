#!/bin/bash
#Please run this script as root!
init=openrc #this is the init system you will be using; options are openrc and runit
localuser=johndoe #need to change this from johndoe, otherwise nothing wil happen; give the name of your user account
hostname=artix #your hostname that you want for this machine

function getdepends(){
pacman --noconfirm -Sy wget
}

#gets the pacman.conf file for artixlinux & removes the need for sig
function pacconffetch() {
wget https://gitea.artixlinux.org/packagesP/pacman/raw/branch/master/trunk/pacman.conf -O /etc/pacman.conf
sed -i 's/Required DatabaseOptional/Never/' /etc/pacman.conf
}

function mirrorsetup(){
mv mirrorlist.bak /etc/pacman.d/mirrorlist
cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.bak
cp /etc/pacman.d/mirrorlist.bak .
#this gets the artix mirrorlist
mv /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist-arch
wget https://gitea.artixlinux.org/packagesA/artix-mirrorlist/raw/branch/master/trunk/mirrorlist -O /etc/pacman.d/mirrorlist
}

function refeshpacdb(){
pacman -Scc && pacman -Syy
}

function getkeys(){
pacman -S artix-keyring
pacman-key --init
pacman-key --populate artix
pacman-key --lsign-key 95AEC5D0C1E294FC9F82B253573A673A53C01BC2
}

function packagedown(){
#downloads (might not be needed, because the last time I ran it I had to download all the files twice because pacman said that they were corrupted)
#may need to remove systemd-dummy related packages, because elogind can replace it
pacman -Sw base base-devel $init-system grub linux linux-headers systemd-dummy systemd-libs-dummy $init netifrc grub mkinitcpio elogind 
}

function removesystemd(){
#remove systemd
pacman -Rdd systemd systemd-libs
}

function packageinst(){
#installs
pacman -S base base-devel $init-system grub linux linux-headers systemd-dummy systemd-libs-dummy $init netifrc grub mkinitcpio elogind
#install some startup stuff
pacman -S --needed acpid-$init alsa-utils-$init autofs-$init cronie-$init fuse-$init openssh-$init syslog-ng-$init networkmanager-$init dbus-$init
}

function initupdate(){
#enables the init
case "$init" in 
  "openrc") rc-update add udev sysinit;;
  "runit") ln -s /etc/runit/sv/udev/ /run/runit/service;;
  *) echo "No valid init system selected";;
esac
}

#grub shouldnt need to be truly reinstalled, just a new config file needs to be made, this should also work cross platform (ie bios and uefi)
function regrub(){
mkinitcpio -P
grub-mkconfig -o /boot/grub/grub.cfg
}

function usergroups(){
if [ $localuser != "johndoe" ]
then
usermod -a -G video,audio,input,power,storage,optical,lp,scanner $localuser
else
echo "NO username given, skipping group add step"
fi
}

function finishingup(){
echo $hostname > /etc/hostname
sed -i 's/= Never/= Required DatabaseOptional/' /etc/pacman.conf #adds security checks back in
#sed -i 's/#rc_parallel="NO"/rc_parallel="YES"/' /etc/rc.conf #this is dependent on openrc being installed
#pacman -S systemd libsystemd
#pacman -S lightdm
#pacman -S sddm xorg-wrapper
#sed -i 's/DISPLAYMANAGER="xdm"/DISPLAYMANAGER="sddm"/' /etc/conf.d/xdm
}

#order that things need to be done 
getdepends
pacconffetch
mirrorsetup
refreshpacdb
getkeys
packagedown
removesystemd
packageinst
initupdate
regrub
usergroups
finishingup

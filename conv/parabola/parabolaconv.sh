#!/bin/bash

pacman --noconfirm -S wget

wget https://git.parabola.nu/abslibre.git/plain/libre/pacman/pacman.conf.x86_64 -O pacman.conf

mv pacman.conf /etc/pacman.conf

sed -i 's/Required DatabaseOptional/Never/' /etc/pacman.conf

pacman -U https://www.parabola.nu/packages/libre/x86_64/parabola-keyring/download
pacman -U https://www.parabola.nu/packages/libre/x86_64/pacman-mirrorlist/download

cp -vr /etc/pacman.d/mirrorlist.pacnew /etc/pacman.d/mirrorlist

pacman -Scc
pacman -Syy
pacman-key --refresh
pacman -Suu pacman your-freedom #could include 'your-privacy' too
#pacman -Syyuu
grub-mkconfig -o /boot/grub/grub.cfg

sed -i '/Never/Required DatabaseOptional/' /etc/pacman.conf

#all you have to do now is reboot
echo After a reboot, you should be in Parabola!

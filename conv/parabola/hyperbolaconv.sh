#!/bin/bash
# You must be running parabola in order to run this script, it uses the hyperbola keyring from parabola
#make the same type of system that I am going to implement in my artix script here with the 
#localuser variable that will add them to all of those groups
localuser=jondoe
hostname=hyperbola

#add some sort of checker that looks at /etc/os-release and sees if you are on parabola, if not the script ends and tells you you have to be running parabola first.

pacman -S hyperbola-keyring

pacman -U https://www.hyperbola.info/packages/core/any/pacman-mirrorlist/download/

cp /etc/pacman.d/mirrorlist.pacnew /etc/pacman.d/mirrorlist

wget https://git.hyperbola.info:50100/packages/core.git/plain/pacman/pacman.conf.x86_64 -O pacman.conf

mv pacman.conf /etc/pacman.conf

pacman -Scc && pacman -Syy
pacman -S pacman
pacman -Suu base

grub-mkconfig -o /boot/grub/grub.cfg

if [ "$localuser" != "johndoe" ] 
then
#usermod -a -G video,network,optical,storage,disk,audio,sys $localuser
gpasswd -a $localuser video  
gpasswd -a $localuser network
gpasswd -a $localuser optical 
gpasswd -a $localuser storage
gpasswd -a $localuser disk 
gpasswd -a $localuser audio 
gpasswd -a $localuser sys
else
echo "no valid user selected"
fi

pacman -Syyuu base

rc-update add cronie default 
rc-update add elogind default
rc-update add NetworkManager default
rc-update add alsasound default

pacman -S polkit

echo "hostname=$hostname" > /etc/conf.d/hostname
echo 'keymap="us"' > /etc/conf.d/keymaps
rc-service keymaps restart

grub-mkconfig -o /boot/grub/grub.cfg

echo "After a reboot you should be in Hyperbola! (And using openrc!)"

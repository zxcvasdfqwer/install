#!/bin/bash

#source : https://sysdfree.wordpress.com/2018/12/22/249/

curl -LO https://framagit.org/Obarun/obarun-mkiso/raw/master/pacman.conf

sed -i 's/SigLevel = Required/SigLevel = Never/' pacman.conf

#mv pacman.conf /etc/pacman.conf

pacman -Syy

pacman -Sw obarun-keyring

pacman -U /var/cache/pacman/pkg/obarun-keyring*

pacman-key --populate obarun archlinux

pacman -S applysys pacopts

pacman -Rnsudd systemd systemd-libs systemd-sysvcompat

pacman -S s6-suite eudev consolekit2

pacopts origin 

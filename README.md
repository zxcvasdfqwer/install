## eli - *easy linux installer* - *GPLv2*
### A collection of scripts that let me automagically install a variety of Linux distros. 

This new README is because there has been a huge change in the main arch script itself, as it now allows for *four* different schemes of install. those being:
* UEFI without encryption
* UEFI with encryption
* Bios without encryption
* Bios with encryption

Everything in this script is as automated as it can possibly be, with even a wrong input checker, but this script is much much more robust than the original, as it now allows for actual customization, versus the one size fits all approach of the last one. The defaults on it are the same as the last one however, so there is no change if you were to run this new script and the old one.

I may also add a function to the script that will automatically detect what type of system it is going to install to (ie, looking for efi specific files on the live dvd that would indicate it needs to be installed in uefi mode) or other such things, possibly an interactive hd selector, even though that would be a little more work than I would want. the script has plenty of opportunity to be optimized, but I still want people to have the option to edit the source directly and make changes there. Mainly offer it as an optional thing for the user to make their life a little more *comfy*. (This is unlikely to be added, however, because I have already made it customizable)

The script now has a *bootstrap* function, which can be replaced with whatever series of commands you need to run to bootstrap a system to /mnt.
You would have to change the install commands in the chroot script, using an alias or something akin to that, but you could likely install gentoo, void, debian, fedora, and any other distro that lets you bootstrap it remotely. 

### Basic Usage

#### Automatic Usage

This repo has a script called eli.sh, which is preprogrammed with a few of the more popular ways to install linux, and all you have to do is call the letter after the script like this:

*eli.sh -e* 

The above command will install arch with encryption for UEFI systems. 


#### Manual Usage

The script now has flags for you to use and they can be called like this:

*install.sh -[flagletter]*

So an example command would be: 

*install.sh -b bios -e yes -d /dev/sdb -f btrfs -c luksroot -n mycomputer*

The options for *-b & -e* are the only ones that will check for errors. Other flag options will not be checking for errors, so read the documentation to know what the correct options are for each flag. 

You can read the help page by running:

*install.sh -h*

## To-Do 

* Add ZFS support, using the archzfs repo and a specialized iso for zfs on root (zfs support will be limited to not messing up your already created zfs vdevs and mount points)
* Make the chroot script not use specific arch commands for installing software (done)
* Test this script with other distributions (notably void and gentoo)
* Rename the script to install.sh instead of archinstall.sh (done)
* Rename this repo to eli, instead of install on gitlab
* Make it dead easy to switch which distro you are going to install (maybe through diffs and patches?)
* Make the help thing in install.sh easier to read (done)
* LVM support?
* Implement regex support to check the other variables
* ReLicense to GPLv2 (done)
* Make patches of the folders for both of the files (chroot and install) so that changing the distro that is installed is as easy as just running a patch. 
* Make all of the scripts POSIX compliant, so that they will work on all UNIX-like systems
* Make it easy to change which partition will become the root partition and boot and so on, so that if people partition their disk differently, the right partition will be mounted to /mnt and /mnt/boot and /mnt/var and so on
* Consider adding the ability to use rEFInd instead of grub on UEFI installs

## Tested Distros
* Arch
* Void (Worked, but with a few bugs, hostname was not properly set, and instpack functions in the chroot script were ignored)

## Untested Distros
* Gentoo
* Debian
* Fedora

## Changing distro
Patches for other distros will be/are included in the distro folders. 

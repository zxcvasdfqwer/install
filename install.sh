#!/bin/bash
while getopts ":b:e:d:f:c:n:l:h" o; do case "${o}" in
	b) boot=${OPTARG} ;;
	e) encrypted=${OPTARG};;
	d) disk=${OPTARG};;
	f) rootfs=${OPTARG};;
	c) cyrptsetuproot=${OPTARG};;
	n) hostname=${OPTARG};;
	l) larbsenabled=${OPTARG};;
	g) gitrepolink=${OPTARG};;
	h) echo -e "Optional Arguments:
        \\n -b: Choose the boot type (options are 'bios' and 'uefi')
        \\n -e: This flag lets you mess around with encryption. Must put a 'yes' or 'no' following it.
        \\n -d: Select the disk in the format of '/dev/sdX' where X is your actual drive letter.
        \\n -f: Choice of root filesystem. Choices are 'ext4', 'jfs', 'xfs' & 'btrfs'
        \\n -c: Set the name of your encryptedroot. Defaults to 'cryptroot'
        \\n -n: Set the hostname of your install
        \\n -l: Decide on whether you want LARBS to run (Will use whatever repo you give it with -g)
        \\n -g: Set the git repo for your dotfiles (for use with larbs)
        \\n -h: Show this message
        \nDefaults:
boot=uefi
encrypted=no
disk=/dev/sda
rootfs=ext4
cryptsetuproot=cryptroot
larbsenabled=no
gitrepolink=https://gitlab.com/LukeSmithxyz/voidrice.git (Luke Smith's dots)
hostname=arch" && exit ;;
	*) echo "-$OPTARG is not a valid option." && exit ;;
esac done

[ -z ${boot+x} ] && export boot=uefi
[ -z ${encrypted+x} ] && export encrypted=no
[ -z ${disk+x} ] && export disk=/dev/sda
[ -z ${rootfs+x} ] && rootfs=ext4
[ -z ${cryptsetuproot+x} ] && export cryptsetuproot=cryptroot
[ -z ${hostname+x} ] && hostname=arch
[ -z ${larbsenabled+x} ] && export larbsenabled=no
[ -z ${gitrepolink+x} ] && export gitrepolink=https://github.com/LukeSmithxyz/voidrice.git
export timezone=America/Chicago
export lang="en_US.UTF-8 UTF-8"
export usepacserve=no

#These two if statements check to see if you have a problem with your boot type and encryption status, and will exit you out of the program if there is a problem with either of them
function inputcheck(){
if [ "$boot" == "uefi" ] || [ "$boot" == "bios" ]
then
	echo "Boot input check passed."
else
	echo Something is wrong with your boot variable, check that first!
exit
fi

if [ "$encrypted" == "yes" ] || [ "$encrypted" == "no" ]
then
	echo "Encryption input check passed."
else
	echo Something is wrong with your encryption answer, check that first!
exit
fi

case "$rootfs" in
	"ext4") echo"Filesystem input check passed." ;;
	"jfs") echo"Filesystem input check passed." ;;
	"xfs")  echo"Filesystem input check passed." ;;
	"btrfs") echo"Filesystem input check passed."  ;;
	"zfs") echo "Not yet..." && exit ;;
	*) echo "Not a valid Filesystem Option!" && exit;;
esac

#if [ "$disk" == "^/dev/[a-z]" ]
#then its good
#else ; its bad

#The same thing can be used for the hostname aswell
#numofchar=$(echo "$cryptroot" | wc -m)
#actualnum=$(($numofchar - 1))
#if [ "$cryptroot" == "^($actualnum)*[a-z]"
#then its good
#else its bad


}
#this command will let you decide on how you want to bootstrap your system, whether that be through pacstrap, debootstrap, getting a tarball, whatever you need to do to bootstrap a system to /mnt, as well as cover any other steps that you need to do
function bootstrap(){
pacstrap /mnt base base-devel linux linux-firmware

	function instpack(){
	pacman --noconfirm --needed -S $1
	}

	function initenable(){
	systemctl enable $1
	}

	function initstart(){
	systemctl start $1
	}

export -f instpack
export -f initenable
export -f initstart
export grubpkgname=grub
}

function partdrive(){
if [ "$boot" == "uefi" ]
then
	if [ "$encrypted" == "yes" ]
	then
cat <<EOF | fdisk $disk
g
n


+100M
t
1

n


+300M
t
2
20

n



t
3
20
w
EOF
	else
cat <<EOF | fdisk $disk
g
n


+100M
t
1

n



t
2
20
w
EOF
	fi
else
	if [ "$encrypted" == "yes" ]
	then
cat <<EOF | fdisk $disk
o
n
p


+100M
b
n

n
p



w
EOF
	else
cat <<EOF | fdisk $disk
o
n
p



w
EOF
	fi
fi
}

function makefs(){
if [ "$boot" == "uefi" ]
then
	mkfs.fat -F32 ${disk}1
	if [ "$encrypted" == "yes" ]
	then
	yes | mkfs.ext4 ${disk}2

		if [ "$rootfs" == "xfs" ] || [ "$rootfs" == "btrfs" ]
		then
			yes | mkfs.$rootfs -f $diskmap
		else
			yes | mkfs.$rootfs $diskmap
		fi
	else
		if [ "$rootfs" == "xfs" ] || [ "$rootfs" == "btrfs" ]
		then
			yes | mkfs.$rootfs -f ${disk}2
		else
			yes | mkfs.$rootfs ${disk}2
		fi
	fi
else
	if [ "$encrypted" == "yes" ]
	then
	yes | mkfs.ext4 ${disk}1

		if [ "$rootfs" == "xfs" ] || [ "$rootfs" == "btrfs" ]
		then
			yes | mkfs.$rootfs -f $diskmap
		else
			yes | mkfs.$rootfs $diskmap
		fi
	else

		if [ "$rootfs" == "xfs" ] || [ "$rootfs" == "btrfs" ]
		then
			yes | mkfs.$rootfs -f ${disk}1
		else
			yes | mkfs.$rootfs ${disk}1
		fi
	fi
fi
}

#This is the function responsible for encrypting the (/) part, of your hd
function encryptroot(){
modprobe dm-crypt
modprobe dm-mod
if [ "$boot" == "uefi" ]
then
	cryptsetup -y -v luksFormat ${disk}3
	cryptsetup open ${disk}3 $cryptsetuproot
else
	cryptsetup -y -v luksFormat ${disk}2
	cryptsetup open ${disk}2 $cryptsetuproot
fi
diskmap=/dev/mapper/$cryptsetuproot
}


function mountpart(){
if [ "$encrypted" == "yes" ]
then
mount $diskmap /mnt
mkdir /mnt/boot
	if [ "$boot" == "uefi" ]
	then
	mount ${disk}2 /mnt/boot
	else
	mount ${disk}1 /mnt/boot
	fi
else
	if [ "$boot" == "uefi" ]
	then
	mount ${disk}2 /mnt
	else
	mount ${disk}1 /mnt
	fi
fi
}

function backupvars () {
backupfile=/mnt/install/varsbackup.sh #this way everything is self containted in that folder
echo '#!/bin/bash' > $backupfile
echo "boot=\"$boot\"" >> $backupfile
echo "encrypted=\"$encrypted\"" >> $backupfile
echo "disk=\"$disk\"" >> $backupfile
echo "rootfs=\"$rootfs\"" >> $backupfile
echo "cryptsetuproot=\"$cryptsetuproot\"" >> $backupfile
echo "hostname=\"$hostname\"" >> $backupfile
echo "larbsenabled=\"$larbsenabled\"" >> $backupfile
echo "gitrepolink=\"$gitrepolink\"" >> $backupfile
echo "timezone=\"$timezone\"" >> $backupfile
echo "lang=\"$lang\"" >> $backupfile
echo "usepacserve=\"$usepacserve\"" >> $backupfile
echo $(declare -f instpack) >> $backupfile
echo $(declare -f initenable) >> $backupfile
echo $(declare -f initstart) >> $backupfile
echo "grubpkgname=\"$grubpkgname\"" >> $backupfile
}

#The actual script begins here
timedatectl set-ntp true
inputcheck
partdrive #if the user selects ZFS for their fs, then thye would need to partition their own drive and do their own filesystem, so partdrive, encryptroot, makefs, and mountpart would all be pointless
if [ "$encrypted" == "yes" ]
then
	encryptroot
else
	echo "Encryption was not selected"
fi
makefs
mountpart
bootstrap

genfstab -U -p /mnt >> /mnt/etc/fstab
echo $hostname >> /mnt/etc/hostname

#copies all the other scripts to /mnt so that they can run
mkdir -p /mnt/install
cp -r * /mnt/install
#chroots into /mnt to finish the install
#should have an option that is set in the bootstrap function that decides on whether to automatically
#run the chroot script, because void gave me problems when I would run the chroot script.
backupvars
arch-chroot /mnt bash install/chroot.sh

#below is encrypted uefi
#echo -e 'g\nn\n\n\n+100M\nt\n1\n\nn\n\n\n+300M\nt\n2\n20\n\nn\n\n\n\nt\n3\n20\nw' | fdisk $disk

#norm uefi below
#echo -e 'g\nn\n\n\n+100M\nt\n1\n\nn\n\n\n\nt\n2\n20\nw'| fdisk $disk

#enc bios below (idk why 'b' is in there?) (b no longer toggles the bootable flag)
#echo -e 'o\nn\np\n\n\n+100M\nb\nn\n\nn\np\n\n\n\nw' | fdisk $disk

#norm bios below
#echo -e 'o\nn\np\n\n\n\nw' | fdisk $disk

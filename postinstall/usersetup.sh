#!/bin/bash
echo "Enter in your new unix username:"
read newuser
echo "Enter in your new account's password:"
read newpass

useradd -m -g wheel -s /bin/bash "${newuser}"
echo "$newuser:$newpass" | chpasswd
unset newpass

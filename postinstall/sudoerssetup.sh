#!/bin/bash

echo "%wheel ALL=(ALL) ALL" >> /etc/sudoers
echo "%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown,/usr/bin/reboot,/usr/bin/systemctl suspend,/usr/bin/wifi-menu,/usr/bin/mount,/usr/bin/umount,/usr/bin/pacman -Su,/usr/bin/pacman -Syu,/usr/bin/systemctl restart,/usr/bin/pacman -Syy" >> /etc/sudoers
echo "You have new permissions in /etc/sudoers"


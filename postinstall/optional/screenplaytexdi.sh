#!/bin/bash
wget mirrors.ctan.org/macros/latex/contrib/screenplay.zip
unzip screenplay.zip
cd screenplay
latex *.ins
latex *.dtx
mkdir /usr/share/texmf-dist/tex/latex/screenplay
cp *.cls /usr/share/texmf-dist/tex/latex/screenplay
texhash
cd ..
rm screenplay.zip

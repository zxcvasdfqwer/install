#!/bin/bash
#an alternative to larbs, no dialog needed!

while getopts ":g:p:h" a; do case "${a}" in 
	g)gitrepo=${OPTARG};;
	p)progfile=${OPTARG};;
	h)echo "This is just a test right now, but this will be the help message. " && exit;;
	*) echo "Not a valid argument" ;;
esac done

# defaults (Variables)
[ -z "$gitrepo" ] && gitrepo="https://gitlab.com/zxcvasdfqwer/dotfiles.git"
[ -z "$progfile" ] && progfile="progs.csv"
aurmanager=pikaur 
#other variables that are used are:
#newuser
#newpass
function setupuser(){
	echo "Enter in your new unix username: "
	read newuser
	echo "Enter in your new unix password (Will echo): "
	read newpass
	useradd -m -g wheel -s /bin/bash "${newuser}"
	usermod -a -G wheel "${newuser}" && mkdir -p /home/"$newuser" && chown "$newuser":wheel /home/"$newuser"
	echo "$newuser:$newpass" | chpasswd
#gets rid of the newpass variable so it can't be used by anything else
	unset newpass
}

function refreshkeyring(){
	pacman --noconfirm -Sy archlinux-keyring
}

function aurmanagerinstall(){
	cd /tmp 
	curl -sO https://aur.archlinux.org/cgit/aur.git/snapshot/"$aurmanager".tar.gz && 
	sudo -u "$newuser" tar -xvf "$aurmanager".tar.gz
	cd "$aurmanager" &&
	sudo -u "$newuser" makepkg --noconfirm -si 
	cd /tmp	
}

function maininstall(){
#pacman --noconfirm --needed -S "$1"
	pacman --noconfirm --needed -S $(cat $progfile | sed '/A,.*/ d' | sed 's/,/ /g' | awk '{print $1}')
}

function aurinstall(){
	pacman --noconfirm --needed -S base-devel git
#this sed command enables all cores for compilation
	sed -i "s/-j2/-j$(nproc)/;s/^#MAKEFLAGS/MAKEFLAGS/" /etc/makepkg.conf
	sudo -u "$newuser" $aurmanager -S --noconfirm $(cat ${progfile} | sed '/A,.*/ !d' | sed 's/,/ /g' | awk '{print $2}')
}

function installprograms(){
	aurmanagerinstall
	maininstall
	aurinstall
}

function pullgitrepo(){
	cd /home/$newuser
	git clone ${gitrepo}
#need to find a way to have it go into the git repo and run 'stow *' (automatically)
	#dot_dir=$(ls) #this could be one way of going about it, because it should be the only thing in your home folder at this time
	echo Enter the name of your dotfiles repo :
	read dot_dir
	sudo -u "$newuser" cd dot_dir && sudo -u "$newuser" stow *
}

function systembeepoff() {
	rmmod pcspkr
	echo "blacklist pcspkr" > /etc/modprobe.d/nobeep.conf
}

function pacmaneyecandy(){
	sed -i "s/^#Color/Color/" /etc/pacman.conf
	sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf
}

function firefoxsetup(){
mkdir -p /home/$newuser/.mozilla/firefox/
cd /home/$newuser/.mozilla/firefox/
sudo -u "$newuser" git clone https://github.com/LukeSmithxyz/mozillarbs.git
}

#Actual Script in order below
refreshkeyring
setupuser
installprograms
pullgitrepo
systembeepoff
pacmaneyecandy
#firefoxsetup
echo "%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown,/usr/bin/reboot,/usr/bin/systemctl suspend,/usr/bin/wifi-menu,/usr/bin/mount,/usr/bin/umount,/usr/bin/pacman -Su,/usr/bin/pacman -Syu,/usr/bin/systemctl restart"

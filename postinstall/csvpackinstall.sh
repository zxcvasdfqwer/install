#!/bin/bash
echo Make sure You dont have any lines that arent needed in your csv file
trizen -S $(cat progs.csv | sed '/A,.*/ !d' | sed 's/,/        /g' | awk '{print $2}')

sudo pacman -S $(cat progs.csv | sed '/A,.*/ d' | sed 's/,/        /g' | awk '{print $1}')

#!/bin/bash
echo "What user's directory do you want to copy these files to?"
read theuser

cp -r . /home/$theuser/
cd /home/$theuser/
#makes sure the user can access these files
#could be changed to 755 (was 777), now 766
chmod 777 * 

echo "Log out and log back in to your user to continue the process"

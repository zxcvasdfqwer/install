#!/bin/bash
#make a tmp directory
mkdir tmp
cd tmp
wget https://developer.apple.com/fonts/downloads/SFPro.zip

unzip SFPro.zip

rm -rf __MACOSX
cd SFPro
bsdtar xvPf 'San Francisco Pro.pkg'
bsdtar xvPf 'San Francisco Pro.pkg/Payload'
cd Library
cd Fonts
cp *.otf ~/.fonts
cd ..
cd ..
cd ..
cd ..
rm -rf tmp

#!/bin/bash
#makes a new directory to store all of the files for ease of management
mkdir tmp
cd tmp
git clone https://github.com/douglascomim/MacOSMOD.git
cd MacOSMOD
rm -rf .git
rm README.md
mkdir -pv ~/.icons
cd ..
cp -r MacOSMOD ~/.icons/
#removes the tmp directory
cd ..
rm -rf tmp

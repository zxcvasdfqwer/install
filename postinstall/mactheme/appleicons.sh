#!/bin/bash
#makes a new directory to store all of the files for ease of management
mkdir tmp
cd tmp
git clone https://github.com/zayronxio/Mojave-CT.git
cd Mojave-CT
rm -rf .git
mkdir -pv ~/.icons
cd ..
cp -r Mojave-CT ~/.icons/
#removes the tmp directory
cd ..
rm -rf tmp

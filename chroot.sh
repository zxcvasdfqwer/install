#!/bin/bash

function inchroot(){
echo "Nothing added to inchroot function!"
}

#sets timezone
function timezoneset(){
ln -sf /usr/share/zoneinfo/$timezone /etc/localtime
hwclock --systohc
}

#adds xyne's repository and installs pacserve for you, arch-based specific
function addxyne(){
if [ "$usepacserve" == "yes" ]
then
	echo '[xyne-x86_64]' >> /etc/pacman.conf
	echo 'SigLevel = Required' >> /etc/pacman.conf
	echo 'Server = https://xyne.archlinux.ca/repos/xyne' >> /etc/pacman.conf
	pacman -Sy
	pacman --noconfirm -S pacserve
	systemctl enable pacserve
	systemctl start pacserve
	#initenable pacserve
	#initstart pacserve
	echo "Pacserve will be running next time you reboot! Make sure you use the 'pacsrv' wrapper"
	echo "or you can put 'Include = /etc/pacman.d/pacserve' under all of the [repo] tags in "
	echo "/etc/pacman.conf"
else
	echo "Pacserve was not enabled"
fi
}

#sets up the locale that you chose
function language(){
	echo "$lang" >> /etc/locale.gen
	locale-gen
}

#network manager setup
function nmsetup(){
#pacman --noconfirm --needed -S networkmanager
#systemctl enable NetworkManager
#systemctl start NetworkManager
	instpack networkmanager
	initenable NetworkManager
	initstart NetworkManager
}

#makes the crypt string that grub uses
function makecryptstring(){
if [ "$boot" == "uefi" ]
then
	cryptstring="cryptdevice=${disk}3:$cryptsetuproot"
else
	cryptstring="cryptdevice=${disk}2:$cryptsetuproot"
fi
}

#modifies /etc/default/grub and mkinitcpio.conf so that they support encryption
function addenc(){
sed -i "s|GRUB_CMDLINE_LINUX=".*"|GRUB_CMDLINE_LINUX=\"$cryptstring\"|" /etc/default/grub
sed -i "s/#GRUB_ENABLE_CRYPTODISK=y/GRUB_ENABLE_CRYPTODISK=y/" /etc/default/grub
sed -i "s/filesystems/encrypt filesystems/g" /etc/mkinitcpio.conf
mkinitcpio -p linux
}

#installs grub for the uefi platform, may want to combine both of the grub functions
#I could move the mount ${disk}1 to above the if statement
function uefigrub(){
#pacman --noconfirm --needed -S efibootmgr os-prober
instpack efibootmgr
instpack os-prober
mkdir /boot/efi
   if [ "$encrypted" == "yes" ]
   then
	mount ${disk}1 /boot/efi
	makecryptstring
	addenc
   else
	echo "Encryption was not selected"
	mount ${disk}1 /boot/efi
   fi
	grub-install --target=x86_64-efi --bootloader-id=GRUB --efi-directory=/boot/efi --recheck
	grub-mkconfig -o /boot/grub/grub.cfg
	mkdir /boot/efi/EFI/BOOT
	cp /boot/efi/EFI/GRUB/grubx64.efi /boot/efi/EFI/BOOT/BOOTX64.EFI
}

#installs grub for the bios platform
function biosgrub(){
mkdir /boot
   if [ "$encrypted" == "yes" ]
   then
	mount ${disk}1 /boot
	makecryptstring
	addenc
   else
	echo "Encryption was not chosen"
   fi
	grub-install --target=i386-pc $disk
	grub-mkconfig -o /boot/grub/grub.cfg
}

function larbsfetch(){
	pacman --noconfirm --needed -S curl
	pacman --noconfirm --needed -S dialog
	curl -LO larbs.xyz/larbs.sh
	#sed -i "s/dialog --infobox \"Installing (neo).*/#/" larbs.sh
	#sed -i "s/(sleep 30 && killall nvim).*/#/" larbs.sh
	#sed -i "s/sudo -u \"\$name\" nvim -E -c .*/#/" larbs.sh
}

#Actual script in order of stuff occurs here
inchroot
timezoneset
#addxyne #xyne's repo is currently broken
language
nmsetup
#pacman --noconfirm --needed -S grub
instpack ${grubpkgname}
case "$boot" in
	"uefi") uefigrub;;
	"bios") biosgrub;;
	*) echo "You shouldn't be here";; #might change this to default to bios as a failsafe?
esac
#pacman --noconfirm --needed -Svim
instpack vim
if [ "$larbsenabled" == "yes" ]
then
larbsfetch
bash larbs.sh -p progs.csv -r ${gitrepolink} -a pikaur
else
echo "My work here is complete"
fi
passwd
